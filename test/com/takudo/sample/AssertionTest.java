package com.takudo.sample;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: doyao
 * Date: 13/04/05
 * Time: 19:19
 */
public class AssertionTest {

	@Test
	public void assertion(){

		String actual = "Hello" + " " + "World";
		assertThat(actual, is("Hello World"));

	}
}
