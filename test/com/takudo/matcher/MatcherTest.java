package com.takudo.matcher;

import static com.takudo.test.junit.matcher.IsDate.dateOf;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: doyao
 * Date: 13/04/08
 * Time: 14:15
 */
public class MatcherTest {

	@Test
	public void isDateで日付が合わない(){

		assertThat(new Date(), is(dateOf(2012, 1, 12)));

	}
}
