package com.takudo.calc;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: doyao
 * Date: 13/04/04
 * Time: 13:46
 */
public class CaluculatorTest {

	@Test
	public void multiplyで乗算結果が取得出来る(){

		Calculator calc = new Calculator();
		int expected = 12;
		int actual = calc.multiply(3,4);

//		fail("まだ実装されていません");

		assertThat(actual, is(expected));

	}

	@Test
	public void multiplyで3と4の乗算結果が取得出来る(){

		Calculator calc = new Calculator();
		int expected = 12;
		int actual = calc.multiply(3,4);

		assertThat(actual, is(expected));

	}

	@Test
	public void multiplyで5と8の乗算結果が取得出来る(){

		Calculator calc = new Calculator();
		int expected = 40;
		int actual = calc.multiply(5,8);

		assertThat(actual, is(expected));

	}

	@Test
	public void divideで3と2の除算結果が取得出来る(){

		Calculator calc = new Calculator();

		Float expected = 1.5f;

		float actual = calc.divide(3,2);

		assertThat(actual, is(expected));

	}

	@Test(expected = IllegalArgumentException.class)
	public void devideで5と0のときIllegalArgumentExceptionを送出する(){
		Calculator calc = new Calculator();
		calc.divide(5,0);
	}


}
